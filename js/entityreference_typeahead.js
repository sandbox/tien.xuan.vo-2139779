(function($) {

  function updateDropdownPosition(dropdown){
    var modal = dropdown.parents('.modal');
    if (modal.length > 0 && modal.is(':visible')) {
      // this hack is required to use the element in modal dialogs.
      // @see http://fusiongrokker.com/post/heavily-customizing-a-bootstrap-typeahead
      if (dropdown.find('.tt-suggestion').length > 0) {
        var pos = dropdown.parents('.col-sm-9').position();
        dropdown.attr('style', 'position: fixed !important; top: '+ (pos.top+92) +'px !important; left: '+ (pos.left+15) +'px !important; ');
      }
      else {
        dropdown.hide();
      }
    }
  }

  Drupal.behaviors.efTypeAhead = {
    attach : function(context, settings) {
      $('.entityreference-typeahead', context).once('typeahead', function() {
        // Arguments for typeahead.
        var typeaheadSearches = new Array();

        var settingId = $(this, context).attr('typeahead-setting-id');

        if (!(settingId in settings.entityreference_typeahead)) {
          return;
        }

        var typeaheadSettings = settings.entityreference_typeahead[settingId];

        // Get typeahead type.
        var typeaheadType = typeaheadSettings.typeahead_type;
        if (typeaheadType instanceof Array) {
          typeaheadType = typeaheadType.pop();
        }

        // Get entity type.
        var entityType = typeaheadSettings.entity_type;
        if (entityType instanceof Array) {
          entityType = entityType.pop();
        }

        // Get bundles.
        var bundles = typeaheadSettings.bundles;
        if (bundles instanceof Array) {
          bundles = bundles.pop();
        }

        // If there is no bundle, we don't have links to typeahead. Stop process
        // here.
        if (!bundles) {
          return;
        }

        // Get field name setting.
        var fieldName = typeaheadSettings.field_name;
        if (fieldName instanceof Array) {
          fieldName = fieldName.pop();
        }

        var basePath = Drupal.settings.basePath;

        for (var bundle in bundles) {
          // Create a typeahead's arg based on bundle.
          var typeaheadSearch = new Object();
          typeaheadSearch.name = 'typeahead-' + settingId + '-' + entityType + '-' + bundle;
          if (Object.keys(bundles).length > 1) {
            typeaheadSearch.header = '<h3>' + bundles[bundle]['label'] + '</h3>';
          }
          typeaheadSearch.valueKey = "key";
          typeaheadSearch.remote = {
            url: basePath + 'entityreference_typeahead/typeahead/' + typeaheadType + '/' + entityType + '/' + bundle + '/' + fieldName + '/%QUERY',
            replace: function(url, query) {
              return url.replace('%QUERY', encodeURIComponent(query));
            },
            filter: function(parsedResponse) {
              var dataset = [];
              if (parsedResponse.length == 0) {
                // No results found. Just create an empty item to show that.
                dataset.push({
                  entity_id: 0,
                  key: "",
                  value: "No results found. Select to reset the query."
                });
              }
              else {
                dataset = parsedResponse;
              }
              return dataset;
            }
          }
          typeaheadSearch.template = bundles[bundle]['template'];
          typeaheadSearch.engine = Hogan;
          typeaheadSearch.limit = 10;

          // Add to args list.
          typeaheadSearches.push(typeaheadSearch);
        }
        $(this, context).each(function(index) {
          if (typeaheadSearches.length > 0) {
            $(this, context).typeahead(typeaheadSearches)
              .on('typeahead:selected', function(evt, item) {
                if (typeaheadType == 'single') {
                  $("input[name='" + settingId + "_typeahead_entity_id']", context).val(item.entity_id);
                  // Set query.
                  if (item.entity_id == 0) {
                    $(this, context).typeahead('setQuery', '');
                  }
                  else {
                    $(this, context).typeahead('setQuery', item.value);
                  }
                }
                else {
                  var prefix = '';
                  if (item.prefix_ids) {
                    prefix = item.prefix_ids + ',';
                  }
                  $("input[name='" + settingId + "_typeahead_entity_id']", context).val(prefix + item.entity_id);

                  // Set query.
                  if (item.prefix) {
                    $(this, context).typeahead('setQuery', item.prefix + item.value);
                  }
                  else {
                    if (item.entity_id == 0) {
                      $(this, context).typeahead('setQuery', '');
                    }
                    else {
                      $(this, context).typeahead('setQuery', item.value);
                    }
                  }
                }
              })
              .on('typeahead:opened', function(evt) {
                var dropdown = $(evt.currentTarget, context).parent().find('.tt-dropdown-menu');

                updateDropdownPosition(dropdown);

                $(evt.currentTarget, context).attr('opened', '1');
              })
              .on('typeahead:closed', function(evt) {
                $(evt.currentTarget).attr('opened', '0');
              });
          }
        });

        // Allow to delete value from field.
        $(this, context).keyup(function() {
          if (!$(this, context).val()) {
            // Empty value.
            $("input[name='" + settingId + "_typeahead_entity_id']", context).val(0);
          }
        });
      });

      // Fix hint style.
      $('.tt-hint', context).addClass('form-text');
      var tt_query = $('.tt-hint', context).next('.tt-query');
      $('.tt-hint', context).attr('maxlength', tt_query.attr('maxlength'));
      $('.tt-hint', context).attr('size', tt_query.attr('size'));

      // Fix typeahead in bootstrap modal.
      // Patch the library first.
      // @see https://drupal.org/node/2269039
      $('span.tt-dropdown-menu', context).on('typeahead:suggestionsRendered', function () {
        var dropdown = $(this, context);

        if (dropdown.parent().find('.tt-query').attr('opened') == '0') {
          return;
        }

        updateDropdownPosition(dropdown);
      });
    }
  };
})(jQuery);
