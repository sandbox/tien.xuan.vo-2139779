
Written by Tien Vo Xuan <tien@go1.com.au>
                           <drupal.org: https://drupal.org/user/2535842>


CONTENTS OF THIS FILE
---------------------

 * INTRODUCTION
 * INSTALLATION
 * USAGE
 * ELEMENT PROPERTIES
 * ROADMAP
 * TODO


INTRODUCTION
------------

This module defines a new form element type, called "entityreference_typeahead",
and 2 new field widget types, called "Typeahead" and "Typeahead (Tags style)"
that allows users to reference any entity by using
http://twitter.github.io/typeahead.js/ widget.
This module is depend on https://drupal.org/project/entityreference.


INSTALLATION
------------

To install the Entity Reference Typeahead module:

 1. Place its entire folder into the "sites/all/modules/contrib" folder of your
    drupal installation.

 2. In your Drupal site, navigate to "admin/modules", search the "Entity
    Reference Typeahead" module, and enable it.

 3. Click on "Save configuration".

 4. Download twitter typeahead library, place its into to "sites/all/libraries".

 5. The following patches need to be applied (manually or by drush make)
    to the twitter typeahead library:
     - http://drupal.org/files/issues/typeahead-suggestionsrendered_event_support-2269039-1.patch


USAGE
-----

Using field widget:

  1. Create new entityreference field.

  2. Change widget type to "Typeahead" or "Typeahead (Tags style)".

Using form element:

  1. Create any form you want in the usual way through drupal_get_form().

  2. Create new form element, specifying 'entityreference_typeahead'
     in the '#type' property of the element. E.g:

        $form['my_entity_reference_typeahead'] = array(
          '#type' => 'entityreference_typeahead',
          '#title' => t('My Reference'),
          '#era_entity_type' => 'user',  // Mandatory.
          '#era_bundles' => array(), // Optional (Any bundle by default).
          '#era_typeahead_type' => "tags", // Optional ("single" By default).
          '#default_value' => array(1, 3, 6), // Optional
        );

  3. When the form is rendered, you should have the entityreference_typeahead
     field ready to use.

  4. For a detailed explanation of the meaning of every '#era_{property}'
     property, see the "ELEMENT PROPERTIES" section of this README.


ELEMENT PROPERTIES
------------------

Explanation of the custom properties used in an 'entityreference_typeahead'
form element, and any Form API standard properties which use might not be
clear:

'#era_entity_type':     The Entity Type to be referenced: Like "user", "node",
                        "comment", "file", "taxonomy_term", etc...

'#era_bundles':         The Bundle to be referenced. For nodes, it would
                        be the content type, like "story", "page", etc...

'#era_typeahead_type':  "single" if you want single value, or "tags" if you want
                        multiple values.

'#default_value':       Default values for form element, it should be as
                        Entity IDs.


ROADMAP
-------

TODO
----
