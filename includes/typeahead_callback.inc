<?php
/**
 * @file
 * typeahead_callback.inc.
 *
 * Typeahead callback for 'entityreference_typeahead' Form API Elements.
 */

/**
 * Menu callback: typeahead the label of an entity.
 *
 * @param $type
 *   The widget type (i.e. 'single' or 'tags').
 * @param $entity_type
 *   The entity type.
 * @param $bundle_name
 *   The bundle name.
 * @param $field_name
 *   The name of the entity-reference field.
 * @param $string
 *   The label of the entity to query by.
 * @todo Use entityreference_get_selection_handler().
 * @see entityreference_autocomplete_callback().
 */
function entityreference_typeahead_typeahead_callback($type, $entity_type, $bundle = '', $field_name = '', $string = '') {
  list($ids, $prefix, $prefix_ids) = entityreference_typeahead_search($type, $entity_type, $bundle, $field_name, urldecode($string));

  if (empty($ids)) {
    $entities = array();
    $results = array();
  }
  else {
    $entities = entity_load($entity_type, $ids);
    $results = entityreference_typeahead_prepare_results($entity_type, $entities, $prefix, $prefix_ids);
  }

  // Let other drupal modules alter the results.
  $context = array(
    'entities' => $entities,
    'entity_type' => $entity_type,
    'bundle' => $bundle,
  );
  drupal_alter('entityreference_typeahead_matches', $results, $context);
  // Finally, output matches in json, as they're are expected by Drupal's ajax.
  drupal_json_output($results);
}

/**
 * Get match ids.
 *
 * @param type $type
 * @param type $entity_type
 * @param type $bundle
 * @param type $field_name
 * @param type $string
 * @return array
 * @see entityreference_typeahead_typeahead_callback().
 */
function entityreference_typeahead_search($type, $entity_type, $bundle = '', $field_name = '', $string = '') {
  $ids = array();

  if ($type == 'tags') {
    // The user enters a comma-separated list of tags. We only process the last tag.
    $tags_typed = entityreference_typeahead_explode_tags($string);
    $tag_last = drupal_strtolower(trim(array_pop($tags_typed)));
    if (!empty($tag_last)) {
      $prefix = count($tags_typed) ? implode(', ', $tags_typed) . ', ' : '';
    }

    // Get prefix ids.
    $prefix_ids = array();
    foreach ($tags_typed as $tag) {
      list($tag_ids, , ) = entityreference_typeahead_search('single', $entity_type, $bundle, $field_name, $tag);
      if (!empty($tag_ids)) {
        $tag_id = reset($tag_ids);
        if (!in_array($tag_id, $prefix_ids)) {
          $prefix_ids[] = $tag_id;
        }
      }
    }
  }
  else {
    // The user enters a single tag.
    $prefix = '';
    $tag_last = drupal_strtolower(trim($string));
    $prefix_ids = array();
  }

  // Get entity metadata, to be used for some checks.
  $entity_info = entity_get_info($entity_type);

  $query = new EntityFieldQuery();
  $query->entityCondition('entity_type', $entity_type);

  // If $bundle is not null, add the 'bundle' condition. Also, save some
  // pain to anyone trying to specify a bundle when the entity has no actual
  // key for bundles.
  if ($bundle  && !empty($entity_info['entity keys']['bundle'])) {
    $query->entityCondition('bundle', $bundle);
  }

  $ids = array_merge($ids, entityreference_typeahead_search_by_properties($query, $tag_last, $entity_type, $bundle));
  $ids = array_merge($ids, entityreference_typeahead_search_by_fields($query, $tag_last, $entity_type, $bundle));

  if ($entity_type == 'user' && user_access('administer users')) {
    // In addition, if the user is administrator, we need to make sure to
    // match the anonymous user, that doesn't actually have a name in the
    // database.
    $anonymous_username = drupal_strtolower(trim(format_username(user_load(0))));
    if (strpos($anonymous_username, $tag_last) !== FALSE) {
      $ids[] = 0;
    }
  }

  $ids = array_unique($ids);
  return array($ids, $prefix, $prefix_ids);
}

/**
 * Clone the query and do the search on each property.
 *
 * @param EntityFieldQuery $query
 * @param string $keyword
 *   Key word to find.
 * @param string $entity_type
 * @param string $bundle
 * @return array
 *   List of entity's id that match the keyword.
 */
function entityreference_typeahead_search_by_properties(EntityFieldQuery $query, $keyword, $entity_type, $bundle) {
  $properties = array();

  foreach (module_implements('entityreference_typeahead_properties') as $module) {
    $properties = array_merge($properties, module_invoke($module, 'entityreference_typeahead_properties', $entity_type, $bundle));
  }

  $ids = array();
  foreach ($properties as $property) {
    $search_property_query = clone $query;
    $search_property_query->propertyCondition($property, $keyword, 'CONTAINS');
    $query_result = $search_property_query->execute();

    if (empty($query_result)) {
      continue;
    }

    $ids = array_merge($ids, array_keys($query_result[$entity_type]));
  }
  return array_unique($ids);
}

/**
 * Clone the query and do the search on each field.
 *
 * @param EntityFieldQuery $query
 * @param string $keyword
 *   Key word to find.
 * @param string $entity_type
 * @param string $bundle
 * @return array
 *   List of entity's id that match the keyword.
 */
function entityreference_typeahead_search_by_fields(EntityFieldQuery $query, $keyword, $entity_type, $bundle) {
  $fields = array();

  foreach (module_implements('entityreference_typeahead_fields') as $module) {
    $fields = array_merge($fields, module_invoke($module, 'entityreference_typeahead_fields', $entity_type, $bundle));
  }

  $ids = array();
  foreach ($fields as $field) {
    $info_field = field_info_field($field);
    if (!isset($info_field['columns']['value'])) {
      continue;
    }

    $search_field_query = clone $query;
    $search_field_query->fieldCondition($field, 'value', $keyword, 'CONTAINS');
    $query_result = $search_field_query->execute();

    if (empty($query_result)) {
      continue;
    }

    $ids = array_merge($ids, array_keys($query_result[$entity_type]));
  }
  return array_unique($ids);
}

/**
 * Prepare results that will be returned to client.
 *
 * @param type $entity_type
 * @param type $ids
 * @return type
 */
function entityreference_typeahead_prepare_results($entity_type, $entities, $prefix, $prefix_ids = array()) {
  $results = array();

  // Iterate through all entities retrieved and process the data to return
  // it as expected by Drupal javascript.
  foreach ($entities as $entity_id => $entity) {
    if (entity_access('view', $entity_type, $entity)) {
      $label = entity_label($entity_type, $entity);
      // Strip things like starting/trailing white spaces, line breaks and tags.
      $label = preg_replace('/\s\s+/', ' ', str_replace("\n", '', trim(decode_entities(strip_tags($label)))));
      // Names containing commas or quotes must be wrapped in quotes.
      if (strpos($label, ',') !== FALSE || strpos($label, '"') !== FALSE) {
        $label = '"' . str_replace('"', '""', $label) . '"';
      }

      // $prefix . $key is the value that will be set in the textfield in
      // the browser, whereas $option is the html that is shown to the user
      // *before* he clicks in one of the options.
      $results[$entity_id] = array(
        'key' => $prefix . $label . ' (' . $entity_id . ')',
        'value' => check_plain($label),
        'entity_id' => $entity_id,
        'prefix_ids' => implode(',', $prefix_ids),
        'prefix' => $prefix,
      );
    }
  }
  return $results;
}
