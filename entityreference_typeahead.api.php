<?php

/**
 * @file
 * API documentation for Entity Reference Typeahead
 */

/**
 * Allows modules to define which properties to find by typeahead.
 *
 * @param $entity_type
 * @param $bundle
 *
 * @return
 *   An array of properties.
 */
function hook_entityreference_typeahead_properties($entity_type, $bundle) {
  $entity_info = entity_get_info($entity_type);

  // Need to fetch the column used as entity label.
  if (isset($entity_info['entity keys']['label'])) {
    $label_column = $entity_info['entity keys']['label'];
    return array($label_column);
  }
  elseif ($entity_type == 'user') {
    return array('name');
  }
  return array();
}

/**
 * Allows modules to define which fields to find by typeahead.
 *
 * @param string $entity_type
 * @param string $bundle
 *
 * @return array
 *  An array of fields.
 */
function hook_entityreference_typeahead_fields($entity_type, $bundle) {
  $fields = array();
  foreach (field_info_instances($entity_type, $bundle) as $name => $field) {
    $details = field_info_field($name);
    if ($details['type'] == 'text') {
      $fields[] = $name;
    }
  }
  return $fields;
}

/**
 * Allows modules to alter the bundle's information before it is sent to
 * javascript.
 *
 * Use this hook to alter the information of bundles before it is sent to
 * typeahead javascript, for example to change bundle's label,
 * or change template display by typeahead.
 *
 * @param array $bundles
 *   An associative array of bundles sent to typeahead javascript.
 * @param string $entity_type
 */
function hook_entityreference_typeahead_bundles_alter(&$bundles, $entity_type) {
  if ($entity_type == 'user' && count($bundles) == 1) {
    $bundle = reset($bundles);
    $bundle['label'] = 'People';
    $bundle['template'] = '<p>'
      . '<img src="{{image}}" width="24" height="24"/> '
      . '<strong>{{name}}</strong> - {{email}}'
      . '</p>';
  }
}

/**
 * Allows modules to alter the ajax results returned by entityreference fields.
 *
 * Use this hook to alter the data returned to users in entityreference fields,
 * for example to add some nice markup to the data, or prevent some results from
 * being returned at all.
 *
 * @param array $matches
 *   An associative array with the results to return in an entityreference field.
 * @param array $context
 *   Array with these keys: entities, entity_type, bundle.
 *
 * @see entityreference_typeahead_typeahead_callback()
 */
function hook_entityreference_typeahead_matches_alter(&$matches, $context) {
  if ($context['entity_type'] != 'user') {
    return;
  }

  foreach ($matches as $id => &$match) {
    $account = $context['entities'][$id];

    $match['name'] = $account->name;
    $match['image'] = theme('user_picture', array('account' => $account));
    $match['url'] = 'user/'. $account->uid;
    $match['email'] = $account->mail;
  }
}
